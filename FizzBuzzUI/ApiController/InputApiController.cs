﻿using Application.IService;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzUI.ApiController
{
    [ApiController]
    [Route("ApiController/[controller]")]
    public class InputApiController:ControllerBase
    {
        private readonly IFizzBuzzService _fizzbuzzService;
        public InputApiController(IFizzBuzzService fizzbuzzService)
        {
            _fizzbuzzService = fizzbuzzService;
        }
        [HttpPost]
        public async Task<ActionResult<int>> Create(Input command)
        {
            return await _fizzbuzzService.TrackInput(command.Number);
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Persistence;

namespace FizzBuzzUI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddPersistence(Configuration);
            
          
            services.AddHttpContextAccessor();            

            // For IIS/ other webserver deployment
            services.Configure<IISServerOptions>(options => options.AutomaticAuthentication = false);

            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddMvc(options => options.EnableEndpointRouting = false)
                 .AddSessionStateTempDataProvider();

            

            services.AddRazorPages();
            
            // Customise default API behaviour
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();                
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseRouting();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                 name: "areaRoute",                 
                 template: "{area:exists}/{controller=Login}/{action=Index}/{id?}"
               );
                routes.MapRoute(
                    name: "default",                    
                    template: "{controller=FizzBuzz}/{action=Index}/{id?}"
                    );
            });
        }



    }
}

using Microsoft.AspNetCore;

namespace FizzBuzzUI
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            await host.RunAsync();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext) =>
            {                
            })           
            .UseStartup<Startup>();
    }
}
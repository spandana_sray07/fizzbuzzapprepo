﻿using Application.IService;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzUI.Controllers
{
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzService _fizzbuzzService;
        public FizzBuzzController(IFizzBuzzService fizzbuzzService)
        {
            _fizzbuzzService = fizzbuzzService;            
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(Input input)
        {
            if (ModelState.IsValid)
            {
                ResultVM result = new ResultVM();
                result = _fizzbuzzService.GetFizzBuzzByNumber(input.Number);
                result.Results = _fizzbuzzService.GetNumbers(input.Number);
                return View("Result", result);
            }
            return View();
        }
    }
}

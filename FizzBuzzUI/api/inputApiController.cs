﻿using Application.IService;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FizzBuzzUI.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class inputApiController : ControllerBase
    {
        private readonly IFizzBuzzService _fizzbuzzService;
        public inputApiController(IFizzBuzzService fizzbuzzService)
        {
            _fizzbuzzService = fizzbuzzService;
        }
        // POST api/<inputApiController>
        [HttpPost]
        public void Post([FromBody] Input command)
        {
            _fizzbuzzService.TrackInput(command.Number);
        }

    }
}

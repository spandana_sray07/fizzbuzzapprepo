﻿function TelNumber(txtTelNo) {
    var flag = true;
    var entered_no = $("#" + txtTelNo).val();
    if (entered_no != '') {
        var Mobile = /^(220)[1-9][0-9]{6,15}$/;
        if (!Mobile.test(entered_no)) {
            flag = false;
            return false;
        }
    }
    return flag;
}
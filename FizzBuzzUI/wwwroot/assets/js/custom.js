/// <reference path="jquery-3.3.1.slim.min.js" />
var host = window.location.host;
var pathInfo = window.location.pathname;
var FN1 = pathInfo.split('/')[1];
var FN2 = pathInfo.split('/')[2];
if (FN1 != '') {
    var appURL = "http://" + host + "/" + FN1;
} else {
    var appURL = "http://" + host;
}

var printMe
var backMe
var deleteMe
var downloadMe
var indicateMe
var excelMe
var pdfMe
var printFooter
// util function
function goBack() {
    window.history.back();
}

//$("#printIcon").hide();
$("#backIcon").hide();
$("#deleteIcon").hide();
$("#indicate").hide();
$("#downloadIcon").hide();
$("#excelIcon").hide();
$("#pdfIcon").hide();

function checkStatus() {
    if (backMe == "yes") {
        $('#backIcon').show();
        $("#backIcon").tooltip();
    }
    if (printMe == "yes") {
        $('#printIcon').show();
        $("#printIcon").tooltip();
    }
    if (deleteMe == "yes") {
        $('#deleteIcon').show();
        $("#deleteIcon").tooltip();
    }

    if (indicateMe == "yes") {
        $('#indicate').show();
        $("#indicate").tooltip();
    }

    if (downloadMe == "yes") {
        $('#downloadIcon').show();
        $("#downloadIcon").tooltip();
    }
    if (excelMe == "yes") {
        $('#excelIcon').show();
        $("#excelIcon").tooltip();
    }
    if (pdfMe == "yes") {
        $('#pdfIcon').show();
        $("#pdfIcon").tooltip();
    }

}

$(document).ready(function () {
    checkStatus();

    $('.form-container')
    var winHeight = $(window).height();
    if ($('.form-container').height() < winHeight) {
        $('.form-container').css({ "min-height": winHeight - 230 })
    }

    $('.searchform').hide();
    $('.searchbtn').click(function () {
        $('.searchform').slideToggle();
        $('.searchbtn .fas').toggleClass('fa-chevron-down fa-chevron-up');
        if ($('.searchbtn span').text() == "Hide")
            $('.searchbtn span').text("Search")
        else
            $('.searchbtn span').text("Hide");
    });
});




/*function loadNavigation(pgName, gLink, pLink, fLink, mLink, lLink, title) {*/



function loadNavigation(pgName, gLink, pLink, fLink, mLink, lLink, title) {
        debugger;
       
    var totLink = '';
    var pathName = window.location.pathname;
    var fileName = pathName.split('/').reverse()[0];
    //alert(pLink + '2');
    //alert(fileName);
    $('.' + pLink).addClass('active');
  
    //alert('active done');
    //alert(gLink + '2');
    //alert(mLink + '3');
    //alert(lLink + '4');
    //alert(title + '5');
    if (pLink != '') {
        $('.' + gLink).addClass('active').find('.dropdown-menu').addClass('show');
    } else {
        $('.' + gLink).addClass('active').find('.dropdown-menu').addClass('show');
    }
   
    if (lLink != '' && mLink != '')
        totLink = "<li class='breadcrumb-item'>" + fLink + " </li><li class='breadcrumb-item'>" + mLink + " </li><li class='breadcrumb-item font-weight-bold'>" + lLink + "</li>";
    else if(mLink != '')
        totLink = " <li class='breadcrumb-item active'> " + fLink + "</li> <li class='breadcrumb-item'> " + mLink + "</li>";
    else
        totLink = " <li class='breadcrumb-item active'> " + fLink + "</li>";    
    $('#navigation').html("<li class='breadcrumb-item'><a id='hd' href='/DashBoard/DashBoard/Index' title='Home' ><i class='fa fa-home'></i></a></li>" + totLink);
    $('#title').append(title);
    printHeader = title;
    cLink = "abc";
}
// print function
function PrintPage() {
    startclock();
    var windowName = "PrintPage";
    var wOption = "width=1000,height=600,menubar=yes,scrollbars=yes,location=no,left=100,top=100";
    var cloneTable = $("#viewTable").clone();
    var head = $('#viewTable thead tr');
   // $('#ViewComponentsec_filter').hide();

    cloneTable.find('input[type=text],select,textarea').each(function () {
        var elementType = $(this).prop('tagName');
        if (elementType == 'SELECT') {
            if ($(this).val() > 0)
                var textVal = $(this).find("option:selected").text();
            else
                textVal = '';
        }
        else
            var textVal = $(this).val();
        $(this).replaceWith('<label>' + textVal + '</label>');
    });
    cloneTable.find('a').each(function () {
        var anchorVal = $(this).html();
        $(this).replaceWith('<label>' + anchorVal + '</label>');
    });

    var pageTitle = $("#title").text();
    var wWinPrint = window.open("", windowName, wOption);
    wWinPrint.document.write("<html><head><script type='text/javascript' src='" + gmainurl + "/assets/js/jquery-3.3.1.slim.min.js'></script>" +
        "<link href='" + gmainurl + "/assets/css/Print.css' rel='stylesheet' /><link href='" + gmainurl +"/assets/css/font-awesome.min.css' rel='stylesheet' />" +
        "<link href='" + gmainurl + "/assets/css/bootstrap.min.css' rel='stylesheet'><title>The Gambia Social Registry Information System(SRIS)</title>" +
        "<style> #ViewComponentsec_filter {display: none;} #ViewComponentsec_length {display: none;} #ViewComponentsec_info {display: none;} #ViewComponentsec_paginate {display: none;} </style>" +
        "</head> <body>");
    wWinPrint.document.write("<div style='padding:5px; margin-bottom:10px; border-bottom:#aeaeae solid 3px;'>" +
        "<img src ='" + gmainurl +"/assets/img/anies.png' alt='National Soil Information System' title ='The Gambia Social Registry Information System(SRIS)' height=100 style='float: left; margin-right: 10px; background-color: #000000;' />" +
        "<div style='float: left;padding-top: 18px;'>" +
        "<h2>The Gambia Social Registry Information System(SRIS)</h2></div><div style='clear: both;'></div></div>");
    wWinPrint.document.write("<div id='printHeader'>" + pageTitle + "</div>");
    wWinPrint.document.write("<div id='printContent'>" + cloneTable.html() + "</div>"); 
    wWinPrint.document.write("<div style='clear:left'><h6><i><div style='float: left;'>&copy; 2021 - The Gambia Social Registry Information System(SRIS)</div><div style='float: right;'><div id='clock'></div>" + printFooter + "</div></i></h6></div>");
    wWinPrint.document.write("</body></html>");
    //$('#ViewComponentsec_filter').hide();
    wWinPrint.document.close();
    wWinPrint.focus();
    return wWinPrint;
}

function startclock() {
    var thetime = new Date();
    var nhours = thetime.getHours();
    var nmins = thetime.getMinutes();
    var nsecn = thetime.getSeconds();
    var nday = thetime.getDay();
    var nmonth = thetime.getMonth();
    var ntoday = thetime.getDate();
    var nyear = thetime.getYear();
    var AorP = " ";

    if (nhours >= 12)
        AorP = "PM";
    else
        AorP = "AM";

    if (nhours >= 13)
        nhours -= 12;

    if (nhours == 0)
        nhours = 12;

    if (nsecn < 10)
        nsecn = "0" + nsecn;

    if (nmins < 10)
        nmins = "0" + nmins;

    if (nday == 0)
        nday = "Sunday";
    if (nday == 1)
        nday = "Monday";
    if (nday == 2)
        nday = "Tuesday";
    if (nday == 3)
        nday = "Wednesday";
    if (nday == 4)
        nday = "Thursday";
    if (nday == 5)
        nday = "Friday";
    if (nday == 6)
        nday = "Saturday";

    nmonth += 1;

    if (nyear <= 99)
        nyear = "19" + nyear;

    if ((nyear > 99) && (nyear < 2000))
        nyear += 1900;
    var monthnameShort = new Array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
    var monthnameFull = new Array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

    printFooter = "" + nday + ", " + monthnameFull[nmonth] + " " + ntoday + ", " + nyear + "  &nbsp;" + nhours + ": " + nmins + " " + AorP + "";

}



// function
(function ($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        };
        //$("#sidebarToggle i").toggleClass("fa-outdent fa-indent");
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('.sidebar .collapse').collapse('hide');
        };
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });


    $(function () {
        $(".service-panel-toggle").on("click", function () {
            $(".customizer").toggleClass("show-service-panel");
        }),
            $(".pagetop").on("click", function () {
                $(".customizer").removeClass("show-service-panel");
            });
    }),



        // Smooth scrolling using jQuery easing
        $(document).on('click', 'a.scroll-to-top', function (e) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top)
            }, 1000, 'easeInOutExpo');
            e.preventDefault();
        });

    $('[data-toggle="tooltip"]').tooltip();

})(jQuery); // End of use strict



// Theme


var lstorageThemeval = localStorage.getItem("webtheme");
var lstorageSideThemeval = localStorage.getItem("sidetheme");

// alert(lstorageThemeval);
if (lstorageThemeval !== "") {

    $('.theme-seeting ul li a').removeClass('active');
    $('head').append('<link id="' + lstorageThemeval + 'css"  href="../assets/css/' + lstorageThemeval + '.css" rel="stylesheet"  />');
    $('#' + lstorageThemeval).addClass('active');
}

if (lstorageThemeval == null) {
    localStorage.removeItem('webtheme');
}

$('.theme-seeting ul li a').on('click', function () {
    $('.theme-seeting ul li a').removeClass('active');
    $(this).addClass('active');

    $('head').find("#" + lstorageThemeval + 'css').remove();
    var themeid = $(this).attr("id");
    //alert(themeid);

    if (typeof (Storage) !== "undefined") {

        localStorage.setItem("webtheme", themeid);
        var lstorageThemeval = localStorage.getItem("webtheme");
        //alert(lstorageThemeval);
        $('head').append('<link id="' + lstorageThemeval + 'css"  href="../assets/css/' + lstorageThemeval + '.css" rel="stylesheet"  />');
    }
});

if (lstorageSideThemeval == null) {
    localStorage.removeItem('sidetheme');
}

if (lstorageSideThemeval !== "") {

    $('.SideNavBG ul li a').removeClass('active');
    $('head').append('<link id="' + lstorageSideThemeval + 'css"  href="../assets/css/' + lstorageSideThemeval + '.css" rel="stylesheet"  />');
    $('#' + lstorageSideThemeval).addClass('active');
}

$('.SideNavBG ul li a').on('click', function () {
    $('.SideNavBG ul li a').removeClass('active');
    $(this).addClass('active');

    $('head').find("#" + lstorageSideThemeval + 'css').remove();
    var themeid = $(this).attr("id");
    //alert(themeid);

    if (typeof (Storage) !== "undefined") {

        localStorage.setItem("sidetheme", themeid);
        var lstorageSideThemeval = localStorage.getItem("sidetheme");
        //alert(lstorageThemeval);
        $('head').append('<link id="' + lstorageSideThemeval + 'css"  href="../assets/css/' + lstorageSideThemeval + '.css" rel="stylesheet"  />');
    }
});

$('#style').on('click', function () {
    localStorage.removeItem('webtheme');
    // alert(lstorageThemeval);
    $('head').find("#" + lstorageThemeval + 'css').remove();
})

function BindRegion(Pageurl, ddlRegion, planid) {
    var url = Pageurl;
    $.getJSON(url, { parentid: planid }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddlRegion).empty();
        $.each(response, function (i, LGA) {
            items += "<option value='" + LGA.value + " '>" + LGA.text + "</option>";
        });
        $("#" + ddlRegion).html(items);
    });
}
function FillDist(URL, RegionID, ddldist) {
    var url = URL;
    $.getJSON(url, { parentid: RegionID }, function (response) {
        var items = "";
        $("#" + ddldist).empty();
        $.each(response, function (i, dist) {
            items += "<option value='" + dist.value + " '>" + dist.text + "</option>";
        });
        $("#" + ddldist).html(items);
    });
}
function BindUser(URL, ddlSuperVisor, userrole) {
    var url = URL;
    $.getJSON(url, { userroleid: userrole }, function (response) {
        var items = "";
        $("#" + ddlSuperVisor).empty();
        $.each(response, function (i, Supervisor) {
            items += "<option value='" + Supervisor.value + " '>" + Supervisor.text + "</option>";
        });
        $("#" + ddlSuperVisor).html(items);
    });
}
function BindDevice(URL, ddldevice) {

    var url = URL;
    $.getJSON(url, function (response) {
        var items = "";
        $("#" + ddldevice).empty();
        $.each(response, function (i, device) {
            items += "<option value='" + device.value + " '>" + device.text + "</option>";
        });
        $("#" + ddldevice).html(items);
    });
}
function BindDistrict(URL, RegionID, ddldist) {
    var url = URL;
    $.getJSON(url, { parentid: RegionID }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddldist).empty();
        $.each(response, function (i, dist) {
            items += "<option value='" + dist.value + " '>" + dist.text + "</option>";
        });
        $("#" + ddldist).html(items);
    });
}
function BindSurvey(URL, LocationID, ddlSurvey, selectedvalue) {
    var url = URL;
    $.getJSON(url, { locationid: LocationID }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddlSurvey).empty();
        $.each(response, function (i, survey) {
            items += "<option value='" + survey.value + " '>" + survey.text + "</option>";
        });
        $("#" + ddlSurvey).html(items);

        var selectObj = document.getElementById(ddlSurvey);
        var valueToSet = selectedvalue;
        if (valueToSet != "") {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].value == parseInt(valueToSet)) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
    });
}
function FillUser(URL, ddlSuperVisor, PLANID, selectedvalue, region) {
    var url = URL;
    $.getJSON(url, { planid: PLANID, regionid: region }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddlSuperVisor).empty();
        $.each(response, function (i, Supervisor) {
            items += "<option value='" + Supervisor.value + " '>" + Supervisor.text + "</option>";
        });
        $("#" + ddlSuperVisor).html(items);
        var selectObj = document.getElementById(ddlSuperVisor);
        var valueToSet = selectedvalue;
        if (valueToSet != "") {
            for (var i = 0; i < selectObj.options.length; i++) {
                if (selectObj.options[i].value == parseInt(valueToSet)) {
                    selectObj.options[i].selected = true;
                    return;
                }
            }
        }
    });
}
function BindAll(URL, ParentID, ddlLocation, Planid) {
    var url = URL;
    $.getJSON(url, { parentid: ParentID, planid: Planid }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddlLocation).empty();
        $.each(response, function (i, location) {            
            items += "<option value='" + location.value + " '>" + location.text + "</option>";
        });
        $("#" + ddlLocation).html(items);
    });
}
function FillDevice(URL, ddldevice, plan) {

    var url = URL;
    $.getJSON(url, { planid: plan }, function (response) {
        var items = "<option value='0'>-Select-</option>";
        $("#" + ddldevice).empty();
        $.each(response, function (i, device) {
            items += "<option value='" + device.value + " '>" + device.text + "</option>";
        });
        $("#" + ddldevice).html(items);
    });
}
function WhiteSpaceCheck(cntr) {
    var strValue = $('#' + cntr).val();
    if (strValue != "") {
        var FistChar = strValue.charAt(0);
        var charlength = strValue.length - 1;
        var lastChar = strValue.charAt(charlength);
        if (/^[a-zA-Z0-9]*$/.test(FistChar) == false) {
            return false;
        }
        if (/^[a-zA-Z0-9.]*$/.test(lastChar) == false) {
            return false;
        }
        else {
            return true;
        }
    }
    else {
        return true;
    }
}

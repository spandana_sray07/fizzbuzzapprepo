﻿$(function () {
    $('input[type="file"]').on("change", function () {
        let filenames = [];
        let files = this.files;
        if (files.length > 1) {
            filenames.push("Total Files (" + files.length + ")");
        } else {
            for (let i in files) {
                if (files.hasOwnProperty(i)) {
                    filenames.push(files[i].name);
                }
            }
        }
        $(this)
            .next(".custom-file-label")
            .html(filenames.join(","));
    });
});
var month = new Array();
month[0] = "Jan";
month[1] = "Feb";
month[2] = "Mar";
month[3] = "Apr";
month[4] = "May";
month[5] = "Jun";
month[6] = "Jul";
month[7] = "Aug";
month[8] = "Sep";
month[9] = "Oct";
month[10] = "Nov";
month[11] = "Dec";

var tdate = new Date();
var dd = tdate.getDate(); //yields day
var MMM = month[tdate.getMonth()]; //yields month
var yyyy = tdate.getFullYear(); //yields year
var curDate = dd + "-" + MMM + "-" + yyyy;
function CheckGreaterDate(cntr, strText) {

    var myDate = $("input#" + cntr).val();
    // alert(myDate + '===' + curDate);
    if (curDate != "") {
        var dateParts = myDate.split("-");
        var newDateStr = dateParts[1] + " " + dateParts[0] + ", " + dateParts[2];
        var cDate = new Date(newDateStr);
        //alert(cDate);
        var dateParts1 = curDate.split("-");
        var newDateStr1 = dateParts1[1] + " " + dateParts1[0] + ", " + dateParts1[2];
        var tdate = new Date(newDateStr1);
        //alert(tdate);
        if (cDate > tdate) {
            return false;
        }
        return true;
    }
}
function CompareDateRange(Controlname1, Controlname2, Fieldname1, Fieldname2) {
    var fromDate = $("input#" + Controlname1).val();
    var toDate = $("input#" + Controlname2).val();
    //alert(fromDate+'==='+toDate);
    if (toDate != "") {
        var dateParts = fromDate.split("-");
        var newDateStr = dateParts[1] + " " + dateParts[0] + ", " + dateParts[2];
        var fdate = new Date(newDateStr);
        // alert(fdate);
        var dateParts1 = toDate.split("-");
        var newDateStr1 = dateParts1[1] + " " + dateParts1[0] + ", " + dateParts1[2];
        var tdate = new Date(newDateStr1);
        // alert(tdate);
        if (fdate > tdate) {

            return false;
        }
        return true;
    }
}
function CheckLessDate(cntr, strText) {
    var myDate = $("input#" + cntr).val();
    var now = new Date();
    //alert(myDate + '===' + curDate);
    if (curDate != "") {
        var dateParts = myDate.split("-");
        var newDateStr = dateParts[1] + " " + dateParts[0] + ", " + dateParts[2];
        var cDate = new Date(newDateStr);
        //alert(cDate);
        var dateParts1 = curDate.split("-");
        var newDateStr1 = dateParts1[1] + " " + dateParts1[0] + ", " + dateParts1[2];
        var tdate = new Date(newDateStr1);
       // alert(tdate);
        if (cDate <= tdate) {

            return false;
        }
        return true;
    }
}

function ValidateDropdown(cntr) {
    var strValue = $('#' + cntr).val();
    if (strValue.length == 0 || strValue == "0") {
        $('#' + cntr).focus();
        return false;
    }
    else
        return true;
}

function BlankTextBox(cntr) {
    var strValue = $('#' + cntr).val();
    if (strValue == "") {
        $('#' + cntr).focus();
        return false;
    }
    else
        return true;
}
function IsSpecialCharacter1stPalce(cntr) {
    var strValue = $('#' + cntr).val();
    // alert(strValue);
    if (strValue != "") {
        var FistChar = strValue.charAt(0);
        if (/^[a-zA-Z0-9]*$/.test(FistChar) == false) {
            return false;
        } else { return true; }
        return true;
    }
    else
        return true;
}

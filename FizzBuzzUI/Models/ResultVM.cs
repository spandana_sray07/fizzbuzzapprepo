﻿namespace FizzBuzzUI.Models
{
    public class ResultVM
    {
        public ResultVM()
        {
            Results = new List<int>();
        }
        public List<int> Results { get; set; }
        public string ResultText { get; set; }
        public string ResultClass { get; set; }
    }
}

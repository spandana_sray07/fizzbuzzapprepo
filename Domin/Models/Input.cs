﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class Input
    {
        

        [Range(1, 1000)]
        public int Number { get; set; }
        
    }
}

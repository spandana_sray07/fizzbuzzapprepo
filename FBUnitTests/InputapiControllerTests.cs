using Application.IService;
using Domain.Models;
using FizzBuzzUI.api;
using Moq;
using Xunit;

namespace FBUnitTests
{
    public class InputapiControllerTests
    {
        #region Property  
        public Mock<IFizzBuzzService> mock = new Mock<IFizzBuzzService>();
        #endregion

        [Fact]
        public void TrackInput()
        {
            var apiController = new inputApiController(mock.Object);
            apiController.Post(new Input() { Number = 5 });
            mock.Verify(x => x.TrackInput(5));
        }
    }
}
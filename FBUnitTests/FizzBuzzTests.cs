﻿using Application.IService;
using Domain.Models;
using FizzBuzzUI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace FBUnitTests
{
    public class FizzBuzzTests
    {
        #region Property  
        public Mock<IFizzBuzzService> mock = new Mock<IFizzBuzzService>();
        #endregion

        [Fact]
        public void GetFizzBuzzText()
        {
            
            mock.Setup(p => p.GetFizzBuzzByNumber(5)).Returns(new ResultVM() { ResultText = "buzz", ResultClass = "Green" });
            FizzBuzzController fb = new FizzBuzzController(mock.Object);
            var result = fb.Index(new Input() { Number = 5 });
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = viewResult.ViewData.Model;
            Assert.NotNull(model);
        }
    }
}

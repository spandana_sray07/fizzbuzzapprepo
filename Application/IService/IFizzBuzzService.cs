﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.IService
{
    public interface IFizzBuzzService
    {
        ResultVM GetFizzBuzzByNumber(int number);
        List<int> GetNumbers(int number);
        Task<int> TrackInput(int number);
    }
}

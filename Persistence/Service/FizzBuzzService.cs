﻿using Application.IService;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Service
{
    public class FizzBuzzService : IFizzBuzzService
    {
        public ResultVM GetFizzBuzzByNumber(int number)
        {
            if (number % 3 == 0 && number % 5 == 0)
            {
                return new ResultVM() { ResultText = (int)DateTime.Today.DayOfWeek == 3 ? "wizz wuzz" : "fizz buzz", ResultClass = "Red" };
            }
            else if (number % 3 == 0)
            {
                return new ResultVM() { ResultText = (int)DateTime.Today.DayOfWeek == 3 ? "wizz" : "fizz", ResultClass = "Blue" };
            }
            else if (number % 5 == 0)
            {
                return new ResultVM() { ResultText = (int)DateTime.Today.DayOfWeek == 3 ? "wuzz" : "buzz", ResultClass = "Green" };
            }
            else
            {
                return new ResultVM() { ResultText = string.Empty, ResultClass = "Red" };
            }
        }

        public List<int> GetNumbers(int number)
        {
            IEnumerable<int> numbers = Enumerable.Range(1, number).Select(n => n);
            return numbers.ToList();

        }

        public Task<int> TrackInput(int number)
        {
            throw new NotImplementedException();
        }
    }
}
